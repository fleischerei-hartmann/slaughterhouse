from django.contrib import admin

from shop.models import ProductVariant
from usermgmt.admin import admin_site


@admin.display(description="PLU", ordering="product__plu")
def product_plu(obj: ProductVariant):
    return obj.product.plu


@admin.display(description="Einheit")
def product_unit(obj: ProductVariant):
    return obj.product.unit


@admin.display(description="Preis", ordering="price")
def price(obj: ProductVariant):
    return f"{obj.price} €"


@admin.register(ProductVariant, site=admin_site)
class ProductVariantAdmin(admin.ModelAdmin):
    search_fields = ("product__plu", "product__name")
    list_display = (product_plu, "product", "amount", product_unit, price)
    list_display_links = ("product",)
    fields = ("product", "amount", product_unit, "price", "created_at")
    autocomplete_fields = ("product",)
    readonly_fields = (product_unit, "created_at")
