from django.contrib import admin

from shop.models import Product, ProductVariant
from usermgmt.admin import admin_site


@admin.display(description="Varianten")
def number_of_variants(obj: Product):
    return obj.variants.count()


class ProductVariantInline(admin.TabularInline):
    model = ProductVariant
    extra = 0
    fields = ("amount", "price")


@admin.register(Product, site=admin_site)
class ProductAdmin(admin.ModelAdmin):
    search_fields = ("plu", "name", "link")
    list_display = ("plu", "name", "link", "unit", number_of_variants)
    list_display_links = ("name",)
    readonly_fields = ("created_at",)
    inlines = (ProductVariantInline,)
