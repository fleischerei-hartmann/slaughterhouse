from django.urls import path

from . import views

urlpatterns = [
    path("", views.ProductListView.as_view(), name="product-list"),
    path("p/<slug:product_slug>/", views.ProductDetailView.as_view(), name="product-detail"),
]
