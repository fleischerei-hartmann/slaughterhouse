from decimal import Decimal, ROUND_DOWN, ROUND_HALF_UP

from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse
from django.utils import timezone

User = get_user_model()


def euro(value: Decimal) -> str:
    eur = str(value.quantize(Decimal("1."), rounding=ROUND_DOWN))
    cent = str(value.quantize(Decimal("0.01"), rounding=ROUND_HALF_UP))[-2:]
    return f"{eur},{cent} €"


class PriceField(models.DecimalField):
    def __init__(self, verbose_name=None, name=None, **kwargs):
        super().__init__(verbose_name, name, **kwargs)
        self.max_digits = 5
        self.decimal_places = 2


class Unit(models.Model):
    name = models.CharField(
        verbose_name="Name",
        max_length=10,
    )
    base_amount = models.PositiveSmallIntegerField(
        verbose_name="Basismenge",
    )

    @property
    def base_amount_str(self) -> str:
        if self.base_amount == 1:
            return self.name
        return f"{self.base_amount} {self.name}"

    def __str__(self):
        return self.name


class Product(models.Model):
    class Meta:
        verbose_name = "Produkt"
        verbose_name_plural = "Produkte"

    name = models.CharField(
        verbose_name="Name",
        max_length=100,
    )
    plu = models.CharField(
        verbose_name="PLU",
        blank=True,
        max_length=4,
    )
    link = models.SlugField(
        verbose_name="Link",
        help_text="eindeutiger Link zum Produkt (nur Buchstaben, Zahlen, - und _)",
        unique=True,
    )
    description = models.TextField(
        verbose_name="Beschreibung",
        help_text="Zutaten etc.",
    )
    unit = models.ForeignKey(
        verbose_name="Einheit",
        to=Unit,
        on_delete=models.PROTECT,
        related_name="+",
    )
    created_at = models.DateTimeField(
        verbose_name="erstellt",
        default=timezone.now,
    )

    @property
    def url(self):
        return reverse("product-detail", kwargs={"product_slug": self.link})

    @property
    def min_price(self) -> Decimal:
        return min(v.price for v in self.variants.all())

    @property
    def max_price(self) -> Decimal:
        return max(v.price for v in self.variants.all())

    @property
    def price_range(self) -> (Decimal, Decimal):
        return self.min_price, self.max_price

    @property
    def price_range_str(self) -> str:
        min_p, max_p = self.price_range
        if min_p == max_p:
            return euro(min_p)
        return f"{euro(min_p)} - {euro(max_p)}"

    @property
    def min_base_price(self) -> Decimal:
        return min(v.base_price for v in self.variants.all())

    @property
    def max_base_price(self) -> Decimal:
        return max(v.base_price for v in self.variants.all())

    @property
    def base_price_range(self) -> (Decimal, Decimal):
        return self.min_base_price, self.max_base_price

    @property
    def base_price_range_str(self) -> str:
        min_p, max_p = self.base_price_range
        if min_p == max_p:
            return euro(min_p)
        return f"{euro(min_p)} - {euro(max_p)}"

    @property
    def base_amount_str(self) -> str:
        return self.unit.base_amount_str

    @property
    def base_price_range_amount_str(self) -> str:
        return f"{self.base_price_range_str} / {self.base_amount_str}"

    @property
    def number_of_variants(self) -> int:
        return len(self.variants.all())

    @property
    def has_single_variant(self) -> bool:
        return self.number_of_variants == 1

    def __str__(self):
        return self.name


class ProductVariant(models.Model):
    class Meta:
        verbose_name = "Produktvariante"
        verbose_name_plural = "Produktvarianten"
        constraints = (models.UniqueConstraint(fields=("product", "amount"), name="unique_product_and_amount"),)

    product = models.ForeignKey(
        verbose_name="Produkt",
        to=Product,
        on_delete=models.CASCADE,
        related_name="variants",
        related_query_name="variant",
    )
    amount = models.PositiveSmallIntegerField(
        verbose_name="Menge",
        help_text="in Bezug auf die Produkteinheit",
    )
    price = PriceField(verbose_name="Preis")
    created_at = models.DateTimeField(verbose_name="erstellt", default=timezone.now)

    @property
    def unit(self) -> Unit:
        return self.product.unit

    @property
    def amount_str(self) -> str:
        return f"{self.amount} {self.unit}"

    @property
    def price_str(self) -> str:
        return euro(self.price)

    @property
    def base_amount(self) -> int:
        return self.unit.base_amount

    @property
    def base_price(self) -> Decimal:
        return self.price / self.amount * self.base_amount

    @property
    def base_amount_str(self) -> str:
        return self.unit.base_amount_str

    @property
    def base_price_str(self) -> str:
        return euro(self.base_price)

    @property
    def base_amount_price_str(self) -> str:
        return f"{self.base_price_str} / {self.base_amount_str}"

    def __str__(self):
        return f"{self.product.name} ({self.amount_str})"
