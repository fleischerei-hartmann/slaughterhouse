from django.http import Http404
from django.views.generic import TemplateView

from shop.models import Product


class ProductListView(TemplateView):
    template_name = "shop/product-list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["products"] = list(Product.objects.all().prefetch_related("variants").order_by("plu"))
        return context


class ProductDetailView(TemplateView):
    template_name = "shop/product-detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            context["product"] = Product.objects.get(link=context["product_slug"])
        except Product.DoesNotExist:
            raise Http404()
        return context
