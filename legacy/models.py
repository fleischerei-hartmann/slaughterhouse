# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class WpActionschedulerActions(models.Model):
    action_id = models.BigAutoField(primary_key=True)
    hook = models.CharField(max_length=191)
    status = models.CharField(max_length=20)
    scheduled_date_gmt = models.DateTimeField(blank=True, null=True)
    scheduled_date_local = models.DateTimeField(blank=True, null=True)
    args = models.CharField(max_length=191, blank=True, null=True)
    schedule = models.TextField(blank=True, null=True)
    group_id = models.PositiveBigIntegerField()
    attempts = models.IntegerField()
    last_attempt_gmt = models.DateTimeField(blank=True, null=True)
    last_attempt_local = models.DateTimeField(blank=True, null=True)
    claim_id = models.PositiveBigIntegerField()
    extended_args = models.CharField(max_length=8000, blank=True, null=True)
    priority = models.PositiveIntegerField()

    class Meta:
        managed = False
        db_table = "wp_actionscheduler_actions"


class WpActionschedulerClaims(models.Model):
    claim_id = models.BigAutoField(primary_key=True)
    date_created_gmt = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_actionscheduler_claims"


class WpActionschedulerGroups(models.Model):
    group_id = models.BigAutoField(primary_key=True)
    slug = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = "wp_actionscheduler_groups"


class WpActionschedulerLogs(models.Model):
    log_id = models.BigAutoField(primary_key=True)
    action_id = models.PositiveBigIntegerField()
    message = models.TextField()
    log_date_gmt = models.DateTimeField(blank=True, null=True)
    log_date_local = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_actionscheduler_logs"


class WpCommentmeta(models.Model):
    meta_id = models.BigAutoField(primary_key=True)
    comment_id = models.PositiveBigIntegerField()
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_commentmeta"


class WpComments(models.Model):
    comment_id = models.BigAutoField(db_column="comment_ID", primary_key=True)  # Field name made lowercase.
    comment_post_id = models.PositiveBigIntegerField(db_column="comment_post_ID")  # Field name made lowercase.
    comment_author = models.TextField()
    comment_author_email = models.CharField(max_length=100)
    comment_author_url = models.CharField(max_length=200)
    comment_author_ip = models.CharField(db_column="comment_author_IP", max_length=100)  # Field name made lowercase.
    comment_date = models.DateTimeField()
    comment_date_gmt = models.DateTimeField()
    comment_content = models.TextField()
    comment_karma = models.IntegerField()
    comment_approved = models.CharField(max_length=20)
    comment_agent = models.CharField(max_length=255)
    comment_type = models.CharField(max_length=20)
    comment_parent = models.PositiveBigIntegerField()
    user_id = models.PositiveBigIntegerField()

    class Meta:
        managed = False
        db_table = "wp_comments"


class WpLinks(models.Model):
    link_id = models.BigAutoField(primary_key=True)
    link_url = models.CharField(max_length=255)
    link_name = models.CharField(max_length=255)
    link_image = models.CharField(max_length=255)
    link_target = models.CharField(max_length=25)
    link_description = models.CharField(max_length=255)
    link_visible = models.CharField(max_length=20)
    link_owner = models.PositiveBigIntegerField()
    link_rating = models.IntegerField()
    link_updated = models.DateTimeField()
    link_rel = models.CharField(max_length=255)
    link_notes = models.TextField()
    link_rss = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = "wp_links"


class WpOptions(models.Model):
    option_id = models.BigAutoField(primary_key=True)
    option_name = models.CharField(unique=True, max_length=191)
    option_value = models.TextField()
    autoload = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = "wp_options"


class WpPostmeta(models.Model):
    meta_id = models.BigAutoField(primary_key=True)
    post_id = models.PositiveBigIntegerField()
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_postmeta"


class WpPosts(models.Model):
    id = models.BigAutoField(db_column="ID", primary_key=True)  # Field name made lowercase.
    post_author = models.PositiveBigIntegerField()
    post_date = models.DateTimeField()
    post_date_gmt = models.DateTimeField()
    post_content = models.TextField()
    post_title = models.TextField()
    post_excerpt = models.TextField()
    post_status = models.CharField(max_length=20)
    comment_status = models.CharField(max_length=20)
    ping_status = models.CharField(max_length=20)
    post_password = models.CharField(max_length=255)
    post_name = models.CharField(max_length=200)
    to_ping = models.TextField()
    pinged = models.TextField()
    post_modified = models.DateTimeField()
    post_modified_gmt = models.DateTimeField()
    post_content_filtered = models.TextField()
    post_parent = models.PositiveBigIntegerField()
    guid = models.CharField(max_length=255)
    menu_order = models.IntegerField()
    post_type = models.CharField(max_length=20)
    post_mime_type = models.CharField(max_length=100)
    comment_count = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = "wp_posts"


class WpStoreabillDocumentItemmeta(models.Model):
    meta_id = models.BigAutoField(primary_key=True)
    storeabill_document_item_id = models.PositiveBigIntegerField()
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_storeabill_document_itemmeta"


class WpStoreabillDocumentItems(models.Model):
    document_item_id = models.BigAutoField(primary_key=True)
    document_item_name = models.TextField()
    document_item_type = models.CharField(max_length=200)
    document_item_parent_id = models.PositiveBigIntegerField()
    document_item_reference_id = models.PositiveBigIntegerField()
    document_item_quantity = models.DecimalField(max_digits=8, decimal_places=3)
    document_id = models.PositiveBigIntegerField()

    class Meta:
        managed = False
        db_table = "wp_storeabill_document_items"


class WpStoreabillDocumentNoticemeta(models.Model):
    meta_id = models.BigAutoField(primary_key=True)
    storeabill_document_notice_id = models.PositiveBigIntegerField()
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_storeabill_document_noticemeta"


class WpStoreabillDocumentNotices(models.Model):
    document_notice_id = models.BigAutoField(primary_key=True)
    document_notice_type = models.CharField(max_length=200)
    document_notice_date_created = models.DateTimeField(blank=True, null=True)
    document_notice_date_created_gmt = models.DateTimeField(blank=True, null=True)
    document_notice_text = models.TextField(blank=True, null=True)
    document_id = models.PositiveBigIntegerField()

    class Meta:
        managed = False
        db_table = "wp_storeabill_document_notices"


class WpStoreabillDocumentmeta(models.Model):
    meta_id = models.BigAutoField(primary_key=True)
    storeabill_document_id = models.PositiveBigIntegerField()
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_storeabill_documentmeta"


class WpStoreabillDocuments(models.Model):
    document_id = models.BigAutoField(primary_key=True)
    document_date_created = models.DateTimeField(blank=True, null=True)
    document_date_created_gmt = models.DateTimeField(blank=True, null=True)
    document_date_modified = models.DateTimeField(blank=True, null=True)
    document_date_modified_gmt = models.DateTimeField(blank=True, null=True)
    document_date_sent = models.DateTimeField(blank=True, null=True)
    document_date_sent_gmt = models.DateTimeField(blank=True, null=True)
    document_date_custom = models.DateTimeField(blank=True, null=True)
    document_date_custom_gmt = models.DateTimeField(blank=True, null=True)
    document_date_custom_extra = models.DateTimeField(blank=True, null=True)
    document_date_custom_extra_gmt = models.DateTimeField(blank=True, null=True)
    document_status = models.CharField(max_length=20)
    document_number = models.CharField(max_length=200)
    document_formatted_number = models.CharField(max_length=200)
    document_customer_id = models.PositiveBigIntegerField()
    document_author_id = models.PositiveBigIntegerField()
    document_country = models.CharField(max_length=2)
    document_index = models.TextField()
    document_relative_path = models.CharField(max_length=260)
    document_reference_id = models.PositiveBigIntegerField()
    document_reference_type = models.CharField(max_length=200)
    document_parent_id = models.PositiveBigIntegerField()
    document_type = models.CharField(max_length=200)
    document_journal_type = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = "wp_storeabill_documents"


class WpStoreabillJournals(models.Model):
    journal_id = models.BigAutoField(primary_key=True)
    journal_type = models.CharField(unique=True, max_length=70)
    journal_name = models.CharField(max_length=200)
    journal_is_archived = models.CharField(max_length=10)
    journal_number_format = models.CharField(max_length=200)
    journal_number_min_size = models.IntegerField()
    journal_last_number = models.PositiveBigIntegerField()
    journal_date_last_reset = models.DateTimeField(blank=True, null=True)
    journal_date_last_reset_gmt = models.DateTimeField(blank=True, null=True)
    journal_reset_interval = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = "wp_storeabill_journals"


class WpTermRelationships(models.Model):
    object_id = models.PositiveBigIntegerField(
        primary_key=True
    )  # The composite primary key (object_id, term_taxonomy_id) found, that is not supported. The first column is selected.
    term_taxonomy_id = models.PositiveBigIntegerField()
    term_order = models.IntegerField()

    class Meta:
        managed = False
        db_table = "wp_term_relationships"
        unique_together = (("object_id", "term_taxonomy_id"),)


class WpTermTaxonomy(models.Model):
    term_taxonomy_id = models.BigAutoField(primary_key=True)
    term_id = models.PositiveBigIntegerField()
    taxonomy = models.CharField(max_length=32)
    description = models.TextField()
    parent = models.PositiveBigIntegerField()
    count = models.BigIntegerField()
    order = models.IntegerField()

    class Meta:
        managed = False
        db_table = "wp_term_taxonomy"
        unique_together = (("term_id", "taxonomy"),)


class WpTermmeta(models.Model):
    meta_id = models.BigAutoField(primary_key=True)
    term_id = models.PositiveBigIntegerField()
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_termmeta"


class WpTerms(models.Model):
    term_id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=200)
    slug = models.CharField(max_length=200)
    term_group = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = "wp_terms"


class WpUsermeta(models.Model):
    umeta_id = models.BigAutoField(primary_key=True)
    user_id = models.PositiveBigIntegerField()
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_usermeta"


class WpUsers(models.Model):
    id = models.BigAutoField(db_column="ID", primary_key=True)  # Field name made lowercase.
    user_login = models.CharField(max_length=60)
    user_pass = models.CharField(max_length=255)
    user_nicename = models.CharField(max_length=50)
    user_email = models.CharField(max_length=100)
    user_url = models.CharField(max_length=100)
    user_registered = models.DateTimeField()
    user_activation_key = models.CharField(max_length=255)
    user_status = models.IntegerField()
    display_name = models.CharField(max_length=250)

    class Meta:
        managed = False
        db_table = "wp_users"


class WpWcAdminNoteActions(models.Model):
    action_id = models.BigAutoField(primary_key=True)
    note_id = models.PositiveBigIntegerField()
    name = models.CharField(max_length=255)
    label = models.CharField(max_length=255)
    query = models.TextField()
    status = models.CharField(max_length=255)
    actioned_text = models.CharField(max_length=255)
    nonce_action = models.CharField(max_length=255, blank=True, null=True)
    nonce_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_wc_admin_note_actions"


class WpWcAdminNotes(models.Model):
    note_id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=255)
    type = models.CharField(max_length=20)
    locale = models.CharField(max_length=20)
    title = models.TextField()
    content = models.TextField()
    content_data = models.TextField(blank=True, null=True)
    status = models.CharField(max_length=200)
    source = models.CharField(max_length=200)
    date_created = models.DateTimeField()
    date_reminder = models.DateTimeField(blank=True, null=True)
    is_snoozable = models.IntegerField()
    layout = models.CharField(max_length=20)
    image = models.CharField(max_length=200, blank=True, null=True)
    is_deleted = models.IntegerField()
    is_read = models.IntegerField()
    icon = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = "wp_wc_admin_notes"


class WpWcCategoryLookup(models.Model):
    category_tree_id = models.PositiveBigIntegerField(
        primary_key=True
    )  # The composite primary key (category_tree_id, category_id) found, that is not supported. The first column is selected.
    category_id = models.PositiveBigIntegerField()

    class Meta:
        managed = False
        db_table = "wp_wc_category_lookup"
        unique_together = (("category_tree_id", "category_id"),)


class WpWcCustomerLookup(models.Model):
    customer_id = models.BigAutoField(primary_key=True)
    user_id = models.PositiveBigIntegerField(unique=True, blank=True, null=True)
    username = models.CharField(max_length=60)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.CharField(max_length=100, blank=True, null=True)
    date_last_active = models.DateTimeField(blank=True, null=True)
    date_registered = models.DateTimeField(blank=True, null=True)
    country = models.CharField(max_length=2)
    postcode = models.CharField(max_length=20)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = "wp_wc_customer_lookup"


class WpWcDownloadLog(models.Model):
    download_log_id = models.BigAutoField(primary_key=True)
    timestamp = models.DateTimeField()
    permission_id = models.PositiveBigIntegerField()
    user_id = models.PositiveBigIntegerField(blank=True, null=True)
    user_ip_address = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_wc_download_log"


class WpWcOrderAddresses(models.Model):
    id = models.BigAutoField(primary_key=True)
    order_id = models.PositiveBigIntegerField()
    address_type = models.CharField(max_length=20, blank=True, null=True)
    first_name = models.TextField(blank=True, null=True)
    last_name = models.TextField(blank=True, null=True)
    company = models.TextField(blank=True, null=True)
    address_1 = models.TextField(blank=True, null=True)
    address_2 = models.TextField(blank=True, null=True)
    city = models.TextField(blank=True, null=True)
    state = models.TextField(blank=True, null=True)
    postcode = models.TextField(blank=True, null=True)
    country = models.TextField(blank=True, null=True)
    email = models.CharField(max_length=320, blank=True, null=True)
    phone = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_wc_order_addresses"
        unique_together = (("address_type", "order_id"),)


class WpWcOrderCouponLookup(models.Model):
    order_id = models.PositiveBigIntegerField(
        primary_key=True
    )  # The composite primary key (order_id, coupon_id) found, that is not supported. The first column is selected.
    coupon_id = models.BigIntegerField()
    date_created = models.DateTimeField()
    discount_amount = models.FloatField()

    class Meta:
        managed = False
        db_table = "wp_wc_order_coupon_lookup"
        unique_together = (("order_id", "coupon_id"),)


class WpWcOrderOperationalData(models.Model):
    id = models.BigAutoField(primary_key=True)
    order_id = models.PositiveBigIntegerField(unique=True, blank=True, null=True)
    created_via = models.CharField(max_length=100, blank=True, null=True)
    woocommerce_version = models.CharField(max_length=20, blank=True, null=True)
    prices_include_tax = models.IntegerField(blank=True, null=True)
    coupon_usages_are_counted = models.IntegerField(blank=True, null=True)
    download_permission_granted = models.IntegerField(blank=True, null=True)
    cart_hash = models.CharField(max_length=100, blank=True, null=True)
    new_order_email_sent = models.IntegerField(blank=True, null=True)
    order_key = models.CharField(max_length=100, blank=True, null=True)
    order_stock_reduced = models.IntegerField(blank=True, null=True)
    date_paid_gmt = models.DateTimeField(blank=True, null=True)
    date_completed_gmt = models.DateTimeField(blank=True, null=True)
    shipping_tax_amount = models.DecimalField(max_digits=26, decimal_places=8, blank=True, null=True)
    shipping_total_amount = models.DecimalField(max_digits=26, decimal_places=8, blank=True, null=True)
    discount_tax_amount = models.DecimalField(max_digits=26, decimal_places=8, blank=True, null=True)
    discount_total_amount = models.DecimalField(max_digits=26, decimal_places=8, blank=True, null=True)
    recorded_sales = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_wc_order_operational_data"


class WpWcOrderProductLookup(models.Model):
    order_item_id = models.PositiveBigIntegerField(primary_key=True)
    order_id = models.PositiveBigIntegerField()
    product_id = models.PositiveBigIntegerField()
    variation_id = models.PositiveBigIntegerField()
    customer_id = models.PositiveBigIntegerField(blank=True, null=True)
    date_created = models.DateTimeField()
    product_qty = models.IntegerField()
    product_net_revenue = models.FloatField()
    product_gross_revenue = models.FloatField()
    coupon_amount = models.FloatField()
    tax_amount = models.FloatField()
    shipping_amount = models.FloatField()
    shipping_tax_amount = models.FloatField()

    class Meta:
        managed = False
        db_table = "wp_wc_order_product_lookup"


class WpWcOrderStats(models.Model):
    order_id = models.PositiveBigIntegerField(primary_key=True)
    parent_id = models.PositiveBigIntegerField()
    date_created = models.DateTimeField()
    date_created_gmt = models.DateTimeField()
    num_items_sold = models.IntegerField()
    total_sales = models.FloatField()
    tax_total = models.FloatField()
    shipping_total = models.FloatField()
    net_total = models.FloatField()
    returning_customer = models.IntegerField(blank=True, null=True)
    status = models.CharField(max_length=200)
    customer_id = models.PositiveBigIntegerField()
    date_paid = models.DateTimeField(blank=True, null=True)
    date_completed = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_wc_order_stats"


class WpWcOrderTaxLookup(models.Model):
    order_id = models.PositiveBigIntegerField(
        primary_key=True
    )  # The composite primary key (order_id, tax_rate_id) found, that is not supported. The first column is selected.
    tax_rate_id = models.PositiveBigIntegerField()
    date_created = models.DateTimeField()
    shipping_tax = models.FloatField()
    order_tax = models.FloatField()
    total_tax = models.FloatField()

    class Meta:
        managed = False
        db_table = "wp_wc_order_tax_lookup"
        unique_together = (("order_id", "tax_rate_id"),)


class WpWcOrders(models.Model):
    id = models.PositiveBigIntegerField(primary_key=True)
    status = models.CharField(max_length=20, blank=True, null=True)
    currency = models.CharField(max_length=10, blank=True, null=True)
    type = models.CharField(max_length=20, blank=True, null=True)
    tax_amount = models.DecimalField(max_digits=26, decimal_places=8, blank=True, null=True)
    total_amount = models.DecimalField(max_digits=26, decimal_places=8, blank=True, null=True)
    customer_id = models.PositiveBigIntegerField(blank=True, null=True)
    billing_email = models.CharField(max_length=320, blank=True, null=True)
    date_created_gmt = models.DateTimeField(blank=True, null=True)
    date_updated_gmt = models.DateTimeField(blank=True, null=True)
    parent_order_id = models.PositiveBigIntegerField(blank=True, null=True)
    payment_method = models.CharField(max_length=100, blank=True, null=True)
    payment_method_title = models.TextField(blank=True, null=True)
    transaction_id = models.CharField(max_length=100, blank=True, null=True)
    ip_address = models.CharField(max_length=100, blank=True, null=True)
    user_agent = models.TextField(blank=True, null=True)
    customer_note = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_wc_orders"


class WpWcOrdersMeta(models.Model):
    id = models.BigAutoField(primary_key=True)
    order_id = models.PositiveBigIntegerField(blank=True, null=True)
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_wc_orders_meta"


class WpWcProductAttributesLookup(models.Model):
    product_id = models.BigIntegerField()
    product_or_parent_id = models.BigIntegerField(
        primary_key=True
    )  # The composite primary key (product_or_parent_id, term_id, product_id, taxonomy) found, that is not supported. The first column is selected.
    taxonomy = models.CharField(max_length=32)
    term_id = models.BigIntegerField()
    is_variation_attribute = models.IntegerField()
    in_stock = models.IntegerField()

    class Meta:
        managed = False
        db_table = "wp_wc_product_attributes_lookup"
        unique_together = (("product_or_parent_id", "term_id", "product_id", "taxonomy"),)


class WpWcProductDownloadDirectories(models.Model):
    url_id = models.BigAutoField(primary_key=True)
    url = models.CharField(max_length=256)
    enabled = models.IntegerField()

    class Meta:
        managed = False
        db_table = "wp_wc_product_download_directories"


class WpWcProductMetaLookup(models.Model):
    product_id = models.BigIntegerField(primary_key=True)
    sku = models.CharField(max_length=100, blank=True, null=True)
    virtual = models.IntegerField(blank=True, null=True)
    downloadable = models.IntegerField(blank=True, null=True)
    min_price = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    max_price = models.DecimalField(max_digits=19, decimal_places=4, blank=True, null=True)
    onsale = models.IntegerField(blank=True, null=True)
    stock_quantity = models.FloatField(blank=True, null=True)
    stock_status = models.CharField(max_length=100, blank=True, null=True)
    rating_count = models.BigIntegerField(blank=True, null=True)
    average_rating = models.DecimalField(max_digits=3, decimal_places=2, blank=True, null=True)
    total_sales = models.BigIntegerField(blank=True, null=True)
    tax_status = models.CharField(max_length=100, blank=True, null=True)
    tax_class = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_wc_product_meta_lookup"


class WpWcRateLimits(models.Model):
    rate_limit_id = models.BigAutoField(primary_key=True)
    rate_limit_key = models.CharField(unique=True, max_length=200)
    rate_limit_expiry = models.PositiveBigIntegerField()
    rate_limit_remaining = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = "wp_wc_rate_limits"


class WpWcReservedStock(models.Model):
    order_id = models.BigIntegerField(
        primary_key=True
    )  # The composite primary key (order_id, product_id) found, that is not supported. The first column is selected.
    product_id = models.BigIntegerField()
    stock_quantity = models.FloatField()
    timestamp = models.DateTimeField()
    expires = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "wp_wc_reserved_stock"
        unique_together = (("order_id", "product_id"),)


class WpWcTaxRateClasses(models.Model):
    tax_rate_class_id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=200)
    slug = models.CharField(unique=True, max_length=200)

    class Meta:
        managed = False
        db_table = "wp_wc_tax_rate_classes"


class WpWcWebhooks(models.Model):
    webhook_id = models.BigAutoField(primary_key=True)
    status = models.CharField(max_length=200)
    name = models.TextField()
    user_id = models.PositiveBigIntegerField()
    delivery_url = models.TextField()
    secret = models.TextField()
    topic = models.CharField(max_length=200)
    date_created = models.DateTimeField()
    date_created_gmt = models.DateTimeField()
    date_modified = models.DateTimeField()
    date_modified_gmt = models.DateTimeField()
    api_version = models.SmallIntegerField()
    failure_count = models.SmallIntegerField()
    pending_delivery = models.IntegerField()

    class Meta:
        managed = False
        db_table = "wp_wc_webhooks"


class WpWoocommerceApiKeys(models.Model):
    key_id = models.BigAutoField(primary_key=True)
    user_id = models.PositiveBigIntegerField()
    description = models.CharField(max_length=200, blank=True, null=True)
    permissions = models.CharField(max_length=10)
    consumer_key = models.CharField(max_length=64)
    consumer_secret = models.CharField(max_length=43)
    nonces = models.TextField(blank=True, null=True)
    truncated_key = models.CharField(max_length=7)
    last_access = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_woocommerce_api_keys"


class WpWoocommerceAttributeTaxonomies(models.Model):
    attribute_id = models.BigAutoField(primary_key=True)
    attribute_name = models.CharField(max_length=200)
    attribute_label = models.CharField(max_length=200, blank=True, null=True)
    attribute_type = models.CharField(max_length=20)
    attribute_orderby = models.CharField(max_length=20)
    attribute_public = models.IntegerField()

    class Meta:
        managed = False
        db_table = "wp_woocommerce_attribute_taxonomies"


class WpWoocommerceDownloadableProductPermissions(models.Model):
    permission_id = models.BigAutoField(primary_key=True)
    download_id = models.CharField(max_length=36)
    product_id = models.PositiveBigIntegerField()
    order_id = models.PositiveBigIntegerField()
    order_key = models.CharField(max_length=200)
    user_email = models.CharField(max_length=200)
    user_id = models.PositiveBigIntegerField(blank=True, null=True)
    downloads_remaining = models.CharField(max_length=9, blank=True, null=True)
    access_granted = models.DateTimeField()
    access_expires = models.DateTimeField(blank=True, null=True)
    download_count = models.PositiveBigIntegerField()

    class Meta:
        managed = False
        db_table = "wp_woocommerce_downloadable_product_permissions"


class WpWoocommerceGzdDhlImProductServices(models.Model):
    product_service_id = models.BigAutoField(primary_key=True)
    product_service_product_id = models.PositiveBigIntegerField()
    product_service_product_parent_id = models.PositiveBigIntegerField()
    product_service_slug = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = "wp_woocommerce_gzd_dhl_im_product_services"


class WpWoocommerceGzdDhlImProducts(models.Model):
    product_id = models.BigAutoField(primary_key=True)
    product_im_id = models.PositiveBigIntegerField()
    product_code = models.IntegerField()
    product_name = models.CharField(max_length=150)
    product_slug = models.CharField(max_length=150)
    product_version = models.IntegerField()
    product_annotation = models.CharField(max_length=500)
    product_description = models.CharField(max_length=500)
    product_information_text = models.TextField()
    product_type = models.CharField(max_length=50)
    product_destination = models.CharField(max_length=20)
    product_price = models.IntegerField()
    product_length_min = models.IntegerField(blank=True, null=True)
    product_length_max = models.IntegerField(blank=True, null=True)
    product_length_unit = models.CharField(max_length=8, blank=True, null=True)
    product_width_min = models.IntegerField(blank=True, null=True)
    product_width_max = models.IntegerField(blank=True, null=True)
    product_width_unit = models.CharField(max_length=8, blank=True, null=True)
    product_height_min = models.IntegerField(blank=True, null=True)
    product_height_max = models.IntegerField(blank=True, null=True)
    product_height_unit = models.CharField(max_length=8, blank=True, null=True)
    product_weight_min = models.IntegerField(blank=True, null=True)
    product_weight_max = models.IntegerField(blank=True, null=True)
    product_weight_unit = models.CharField(max_length=8, blank=True, null=True)
    product_parent_id = models.PositiveBigIntegerField()
    product_service_count = models.IntegerField()
    product_is_wp_int = models.IntegerField()

    class Meta:
        managed = False
        db_table = "wp_woocommerce_gzd_dhl_im_products"


class WpWoocommerceGzdPackaging(models.Model):
    packaging_id = models.BigAutoField(primary_key=True)
    packaging_date_created = models.DateTimeField(blank=True, null=True)
    packaging_date_created_gmt = models.DateTimeField(blank=True, null=True)
    packaging_type = models.CharField(max_length=200)
    packaging_description = models.TextField()
    packaging_weight = models.DecimalField(max_digits=6, decimal_places=2)
    packaging_order = models.PositiveBigIntegerField()
    packaging_max_content_weight = models.DecimalField(max_digits=6, decimal_places=2)
    packaging_length = models.DecimalField(max_digits=6, decimal_places=2)
    packaging_width = models.DecimalField(max_digits=6, decimal_places=2)
    packaging_height = models.DecimalField(max_digits=6, decimal_places=2)

    class Meta:
        managed = False
        db_table = "wp_woocommerce_gzd_packaging"


class WpWoocommerceGzdPackagingmeta(models.Model):
    meta_id = models.BigAutoField(primary_key=True)
    gzd_packaging_id = models.PositiveBigIntegerField()
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_woocommerce_gzd_packagingmeta"


class WpWoocommerceGzdShipmentItemmeta(models.Model):
    meta_id = models.BigAutoField(primary_key=True)
    gzd_shipment_item_id = models.PositiveBigIntegerField()
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_woocommerce_gzd_shipment_itemmeta"


class WpWoocommerceGzdShipmentItems(models.Model):
    shipment_item_id = models.BigAutoField(primary_key=True)
    shipment_id = models.PositiveBigIntegerField()
    shipment_item_name = models.TextField()
    shipment_item_order_item_id = models.PositiveBigIntegerField()
    shipment_item_product_id = models.PositiveBigIntegerField()
    shipment_item_parent_id = models.PositiveBigIntegerField()
    shipment_item_quantity = models.PositiveSmallIntegerField()

    class Meta:
        managed = False
        db_table = "wp_woocommerce_gzd_shipment_items"


class WpWoocommerceGzdShipmentLabelmeta(models.Model):
    meta_id = models.BigAutoField(primary_key=True)
    gzd_shipment_label_id = models.PositiveBigIntegerField()
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_woocommerce_gzd_shipment_labelmeta"


class WpWoocommerceGzdShipmentLabels(models.Model):
    label_id = models.BigAutoField(primary_key=True)
    label_date_created = models.DateTimeField(blank=True, null=True)
    label_date_created_gmt = models.DateTimeField(blank=True, null=True)
    label_shipment_id = models.PositiveBigIntegerField()
    label_parent_id = models.PositiveBigIntegerField()
    label_number = models.CharField(max_length=200)
    label_product_id = models.CharField(max_length=200)
    label_shipping_provider = models.CharField(max_length=200)
    label_path = models.CharField(max_length=200)
    label_type = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = "wp_woocommerce_gzd_shipment_labels"


class WpWoocommerceGzdShipmentmeta(models.Model):
    meta_id = models.BigAutoField(primary_key=True)
    gzd_shipment_id = models.PositiveBigIntegerField()
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_woocommerce_gzd_shipmentmeta"


class WpWoocommerceGzdShipments(models.Model):
    shipment_id = models.BigAutoField(primary_key=True)
    shipment_date_created = models.DateTimeField(blank=True, null=True)
    shipment_date_created_gmt = models.DateTimeField(blank=True, null=True)
    shipment_date_sent = models.DateTimeField(blank=True, null=True)
    shipment_date_sent_gmt = models.DateTimeField(blank=True, null=True)
    shipment_est_delivery_date = models.DateTimeField(blank=True, null=True)
    shipment_est_delivery_date_gmt = models.DateTimeField(blank=True, null=True)
    shipment_status = models.CharField(max_length=20)
    shipment_order_id = models.PositiveBigIntegerField()
    shipment_packaging_id = models.PositiveBigIntegerField()
    shipment_parent_id = models.PositiveBigIntegerField()
    shipment_country = models.CharField(max_length=2)
    shipment_tracking_id = models.CharField(max_length=200)
    shipment_type = models.CharField(max_length=200)
    shipment_version = models.CharField(max_length=200)
    shipment_search_index = models.TextField()
    shipment_shipping_provider = models.CharField(max_length=200)
    shipment_shipping_method = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = "wp_woocommerce_gzd_shipments"


class WpWoocommerceGzdShippingProvider(models.Model):
    shipping_provider_id = models.BigAutoField(primary_key=True)
    shipping_provider_activated = models.IntegerField()
    shipping_provider_title = models.CharField(max_length=200)
    shipping_provider_name = models.CharField(unique=True, max_length=191)
    shipping_provider_order = models.SmallIntegerField()

    class Meta:
        managed = False
        db_table = "wp_woocommerce_gzd_shipping_provider"


class WpWoocommerceGzdShippingProvidermeta(models.Model):
    meta_id = models.BigAutoField(primary_key=True)
    gzd_shipping_provider_id = models.PositiveBigIntegerField()
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_woocommerce_gzd_shipping_providermeta"


class WpWoocommerceLog(models.Model):
    log_id = models.BigAutoField(primary_key=True)
    timestamp = models.DateTimeField()
    level = models.SmallIntegerField()
    source = models.CharField(max_length=200)
    message = models.TextField()
    context = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_woocommerce_log"


class WpWoocommerceOrderItemmeta(models.Model):
    meta_id = models.BigAutoField(primary_key=True)
    order_item_id = models.PositiveBigIntegerField()
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_woocommerce_order_itemmeta"


class WpWoocommerceOrderItems(models.Model):
    order_item_id = models.BigAutoField(primary_key=True)
    order_item_name = models.TextField()
    order_item_type = models.CharField(max_length=200)
    order_id = models.PositiveBigIntegerField()

    class Meta:
        managed = False
        db_table = "wp_woocommerce_order_items"


class WpWoocommercePaymentTokenmeta(models.Model):
    meta_id = models.BigAutoField(primary_key=True)
    payment_token_id = models.PositiveBigIntegerField()
    meta_key = models.CharField(max_length=255, blank=True, null=True)
    meta_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "wp_woocommerce_payment_tokenmeta"


class WpWoocommercePaymentTokens(models.Model):
    token_id = models.BigAutoField(primary_key=True)
    gateway_id = models.CharField(max_length=200)
    token = models.TextField()
    user_id = models.PositiveBigIntegerField()
    type = models.CharField(max_length=200)
    is_default = models.IntegerField()

    class Meta:
        managed = False
        db_table = "wp_woocommerce_payment_tokens"


class WpWoocommerceSessions(models.Model):
    session_id = models.BigAutoField(primary_key=True)
    session_key = models.CharField(unique=True, max_length=32)
    session_value = models.TextField()
    session_expiry = models.PositiveBigIntegerField()

    class Meta:
        managed = False
        db_table = "wp_woocommerce_sessions"


class WpWoocommerceShippingZoneLocations(models.Model):
    location_id = models.BigAutoField(primary_key=True)
    zone_id = models.PositiveBigIntegerField()
    location_code = models.CharField(max_length=200)
    location_type = models.CharField(max_length=40)

    class Meta:
        managed = False
        db_table = "wp_woocommerce_shipping_zone_locations"


class WpWoocommerceShippingZoneMethods(models.Model):
    zone_id = models.PositiveBigIntegerField()
    instance_id = models.BigAutoField(primary_key=True)
    method_id = models.CharField(max_length=200)
    method_order = models.PositiveBigIntegerField()
    is_enabled = models.IntegerField()

    class Meta:
        managed = False
        db_table = "wp_woocommerce_shipping_zone_methods"


class WpWoocommerceShippingZones(models.Model):
    zone_id = models.BigAutoField(primary_key=True)
    zone_name = models.CharField(max_length=200)
    zone_order = models.PositiveBigIntegerField()

    class Meta:
        managed = False
        db_table = "wp_woocommerce_shipping_zones"


class WpWoocommerceTaxRateLocations(models.Model):
    location_id = models.BigAutoField(primary_key=True)
    location_code = models.CharField(max_length=200)
    tax_rate_id = models.PositiveBigIntegerField()
    location_type = models.CharField(max_length=40)

    class Meta:
        managed = False
        db_table = "wp_woocommerce_tax_rate_locations"


class WpWoocommerceTaxRates(models.Model):
    tax_rate_id = models.BigAutoField(primary_key=True)
    tax_rate_country = models.CharField(max_length=2)
    tax_rate_state = models.CharField(max_length=200)
    tax_rate = models.CharField(max_length=8)
    tax_rate_name = models.CharField(max_length=200)
    tax_rate_priority = models.PositiveBigIntegerField()
    tax_rate_compound = models.IntegerField()
    tax_rate_shipping = models.IntegerField()
    tax_rate_order = models.PositiveBigIntegerField()
    tax_rate_class = models.CharField(max_length=200)

    class Meta:
        managed = False
        db_table = "wp_woocommerce_tax_rates"


class WpWpmailsmtpDebugEvents(models.Model):
    content = models.TextField(blank=True, null=True)
    initiator = models.TextField(blank=True, null=True)
    event_type = models.PositiveIntegerField()
    created_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "wp_wpmailsmtp_debug_events"


class WpWpmailsmtpTasksMeta(models.Model):
    id = models.BigAutoField(primary_key=True)
    action = models.CharField(max_length=255)
    data = models.TextField()
    date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "wp_wpmailsmtp_tasks_meta"
