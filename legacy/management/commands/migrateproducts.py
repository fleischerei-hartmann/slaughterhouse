from decimal import Decimal

from django.core.management import BaseCommand
from django.db import transaction

from legacy.models import WpPosts, WpPostmeta
from shop.models import Product, ProductVariant, Unit


def parse_product_attributes(value: str) -> list[int]:
    or_idx = value.find("|")
    start = value.rfind('"', 0, or_idx) + 1
    end = value.find('"', or_idx)
    amounts_str = value[start:end]

    amounts = []
    for amount_str in amounts_str.split("|"):
        stripped = "".join(char for char in amount_str if char.isnumeric())
        amounts.append(int(stripped))
    return sorted(amounts)


class Command(BaseCommand):
    @transaction.atomic()
    def handle(self, *args, **options):
        unit_by_name = {
            "g": Unit.objects.create(name="g", base_amount=100),
            "Stück": Unit.objects.create(name="Stück", base_amount=1),
            "glas": Unit.objects.create(name="Glas", base_amount=1),
        }
        for post in WpPosts.objects.filter(post_type="product", post_status="publish"):
            po_meta = WpPostmeta.objects.filter(post_id=post.id)

            unit_name = po_meta.get(meta_key="_unit").meta_value
            unit = unit_by_name[unit_name]

            desc = (
                "\n\n".join((post.post_excerpt.strip(), post.post_content.strip()))
                .replace("\r\n", "\n")
                .replace("&nbsp;", "")
                .replace("\xa0", " ")
                .strip()
            )
            plu = po_meta.get(meta_key="_sku").meta_value
            product = Product.objects.create(
                name=post.post_title,
                plu=plu,
                link=post.post_name,
                description=desc,
                unit=unit,
                created_at=post.post_date,
            )

            variants = []
            prices = po_meta.filter(meta_key="_price").values_list("meta_value", flat=True)
            if len(prices) == 1:
                amount = po_meta.get(meta_key="_unit_product").meta_value
                variants.append(
                    ProductVariant.objects.create(
                        product=product,
                        amount=int(amount),
                        price=Decimal(prices[0]),
                        created_at=product.created_at,
                    )
                )
            else:
                amounts = parse_product_attributes(po_meta.get(meta_key="_product_attributes").meta_value)
                prices = sorted(Decimal(p) for p in prices)
                assert len(amounts) == len(prices)
                for idx in range(len(amounts)):
                    variants.append(
                        ProductVariant.objects.create(
                            product=product,
                            amount=amounts[idx],
                            price=prices[idx],
                            created_at=product.created_at,
                        )
                    )

            # small correction for the unit
            if product.unit.name == "g" and all(v.amount < 20 for v in variants):
                product.unit = unit_by_name["Stück"]
                product.save()

        print(f"migrated {Product.objects.count()} products and {ProductVariant.objects.count()} variants")
