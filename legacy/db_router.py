class LegacyDBRouter:
    def db_for_read(self, model, **hints):
        if model._meta.app_label == "legacy":
            return "legacy"
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label == "legacy":
            raise AssertionError("legacy db is not writable")
        return None

    def allow_relation(self, obj1, obj2, **hints):
        if obj1._meta.app_label == "legacy" or obj2._meta.app_label == "legacy":
            raise AssertionError("relations to/from legacy db models are not allowed")
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if db == "legacy":  # block migrations on the legacy db
            return False
        if app_label == "legacy":  # block legacy migrations on other dbs
            return False
        return None
