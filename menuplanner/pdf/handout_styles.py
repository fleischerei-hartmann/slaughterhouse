import os

from django.conf import settings
from reportlab.lib.colors import HexColor, black
from reportlab.lib.enums import TA_RIGHT, TA_LEFT
from reportlab.lib.pagesizes import landscape, A4, portrait, A5
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.units import mm
from reportlab.platypus import Image
from reportlab.platypus import TableStyle

from menuplanner.utils import get_handout_a5_file_path, get_handout_a4_file_path

butchery_red = HexColor("#99131C")
dark_gray = HexColor("#666666")
light_gray = HexColor("#CCCCCC")

_logo_orig_width = 453
_logo_orig_height = 209


def _pgr(nm, sz, ld=None, al=TA_LEFT, cl=black):
    ld = ld or sz
    return ParagraphStyle(
        name=nm,
        fontSize=sz,
        leading=ld if ld else sz,
        alignment=al,
        textColor=cl,
    )


def _get_logo_image(width, **kwargs):
    height = width / _logo_orig_width * _logo_orig_height
    return Image(
        filename=os.path.join(settings.BASE_DIR, "menuplanner", "pdf", "logo.png"),
        width=width,
        height=height,
        **kwargs,
    )


class StyleSet:
    file_path = None
    page_margin = 12 * mm
    page_size = portrait(A4)

    logo_width = 70 * mm
    header_text_size = 18
    header_date_size = 24
    header_table_style = TableStyle(
        [
            ("VALIGN", (0, 0), (-1, -1), "MIDDLE"),
            ("LEFTPADDING", (0, 0), (-1, -1), 0),
            ("RIGHTPADDING", (0, 0), (-1, -1), 0),
        ]
    )

    pre_weekday_space = 3 * mm
    content_text_size = 18
    content_table_space_before = 4 * mm
    content_table_space_after = 8 * mm
    content_table_style = TableStyle(
        [
            ("VALIGN", (0, 0), (-1, -1), "TOP"),
            ("LEFTPADDING", (0, 0), (-1, -1), 0),
            ("RIGHTPADDING", (0, 0), (-1, -1), 0),
        ]
    )

    small_text_size = 12

    @property
    def document_properties(self):
        return {
            "filename": self.file_path,
            "pagesize": self.page_size,
            "leftMargin": self.page_margin,
            "topMargin": self.page_margin,
            "rightMargin": self.page_margin,
            "bottomMargin": self.page_margin,
        }

    @property
    def single_page_width(self):
        return self.page_size[0] - (2 * self.page_margin)

    @property
    def logo_image(self):
        return _get_logo_image(self.logo_width, hAlign="LEFT")

    @property
    def header_text_style(self):
        return _pgr("header-text", sz=self.header_text_size, ld=self.header_text_size * 1.4, al=TA_RIGHT)

    @property
    def header_date_style(self):
        return _pgr("header-text", sz=self.header_date_size, ld=self.header_date_size * 1.4, al=TA_RIGHT)

    @property
    def header_table_col_widths(self):
        return [self.logo_width * 1.2, None]

    @property
    def weekday_text_style(self):
        return _pgr("weekday-text", sz=self.content_text_size, ld=self.header_text_size * 1.1, cl=butchery_red)

    @property
    def normal_text_style(self):
        return _pgr("normal-text", sz=self.content_text_size, ld=self.header_text_size * 1.1)

    @property
    def price_text_style(self):
        return _pgr("price-text", sz=self.content_text_size, ld=self.header_text_size * 1.1, al=TA_RIGHT)

    @property
    def content_table_col_widths(self):
        return [self.single_page_width * 0.82, None]

    @property
    def small_text_style(self):
        return _pgr("small-text", sz=self.small_text_size, ld=self.small_text_size, cl=dark_gray)


class A4StyleSet(StyleSet):
    file_path = get_handout_a4_file_path()


_f = 0.7


class DoubleA5StyleSet(StyleSet):
    file_path = get_handout_a5_file_path()
    page_margin = 6 * mm
    page_size = landscape(A4)

    logo_width = _f * StyleSet.logo_width
    header_text_size = _f * StyleSet.header_text_size
    header_date_size = _f * StyleSet.header_date_size

    pre_weekday_space = _f * StyleSet.pre_weekday_space
    content_text_size = _f * StyleSet.content_text_size
    content_table_space_before = _f * StyleSet.content_table_space_before
    content_table_space_after = _f * StyleSet.content_table_space_after

    small_text_size = _f * StyleSet.small_text_size

    # special layout for double A5 page
    surrounding_table_style = TableStyle([("LINEBEFORE", (2, 0), (2, -1), 0.5, light_gray)])

    @property
    def single_page_width(self):
        return A5[0] - (2 * self.page_margin)

    @property
    def surrounding_table_col_widths(self):
        return [self.single_page_width, self.page_margin, self.page_margin, self.single_page_width]
