from typing import List

from core.enums import Weekday
from reportlab.lib.enums import TA_CENTER
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle
from reportlab.platypus import Paragraph, TableStyle, Table, SimpleDocTemplate

from menuplanner.models import Dish
from menuplanner.utils import get_list_file_path

header_style = ParagraphStyle(
    "header",
    fontSize=17,
    leading=18,
    alignment=TA_CENTER,
    spaceAfter=4,
)
subheader_style = ParagraphStyle(
    "subheader",
    fontSize=14,
    leading=15,
    alignment=TA_CENTER,
    spaceAfter=4,
)
normal_style = ParagraphStyle(
    "normal",
    fontSize=9,
    leading=10,
)


def generate_list(dishes: List[Dish]):
    content = [
        Paragraph("<b>Speiseplan</b>", header_style),
    ]

    table_data = []
    current_week_number = None
    for dish in dishes:
        row = []

        if dish.week_number != current_week_number:
            current_week_number = dish.week_number
            row.append(Paragraph(f"<b>Woche {dish.week_number}</b>", normal_style))
        else:
            row.append(Paragraph("", normal_style))

        row.append(Paragraph(Weekday(dish.weekday).label[:2], normal_style))

        row.append(Paragraph(dish.name, normal_style))

        row.append(
            Paragraph(
                f"{dish.get_price_string()} <font size=8>/ {dish.get_small_price_string()}</font>",
                normal_style,
            )
        )

        table_data.append(row)

    top_left = (0, 0)
    bottom_right = (-1, -1)
    table_style = TableStyle(
        [
            ("GRID", top_left, bottom_right, 0.6, "#cccccc"),
            ("VALIGN", top_left, bottom_right, "middle"),
        ]
    )
    content.append(
        Table(
            data=table_data,
            style=table_style,
            colWidths=["13%", "6%", "65%", "16%"],
        )
    )

    doc = SimpleDocTemplate(
        get_list_file_path(),
        pagesize=A4,
        leftMargin=20,
        topMargin=8,
        rightMargin=20,
        bottomMargin=8,
    )
    doc.build(content)
