from typing import List

from reportlab.lib.enums import TA_CENTER
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle
from reportlab.platypus import SimpleDocTemplate, Table, Paragraph, TableStyle, PageBreak

from menuplanner.models import Dish
from menuplanner.utils import get_postings_file_path, chunked

main_style = ParagraphStyle(
    "main",
    fontSize=44,
    leading=48,
    alignment=TA_CENTER,
)
sub_style = ParagraphStyle(
    "sub",
    fontSize=36,
    leading=40,
    alignment=TA_CENTER,
)
cut_line_style = ParagraphStyle(
    "cut-line",
    fontSize=10,
    leading=12,
    alignment=TA_CENTER,
)


def _create_single_dish_table_data(dish):
    return [
        [Paragraph(f"<b>{dish.name}</b>", main_style)],
        [Paragraph(f"<b>{dish.get_price_string()}</b>", main_style)],
        [Paragraph(f"<b>kleine Portion - {dish.get_small_price_string()}</b>", sub_style)],
    ]


total_height = A4[1] - 36
name_height = 0.24 * total_height
price_height = 0.10 * total_height
small_portion_height = 0.14 * total_height
cut_line_height = 0.04 * total_height


def generate_postings(dishes: List[Dish]):
    top_left = (0, 0)
    bottom_right = (-1, -1)
    table_style = TableStyle(
        [
            ("GRID", (0, 3), (0, 3), 1, "#cccccc"),
            ("VALIGN", top_left, bottom_right, "middle"),
        ]
    )

    content = []
    for dish_chunk in chunked(dishes, 2):
        table_data = _create_single_dish_table_data(dish_chunk[0])
        table_data += ([[Paragraph("<i>diesen Bereich wegschneiden</i>", cut_line_style)]],)
        row_heights = [name_height, price_height, small_portion_height, cut_line_height]
        if len(dish_chunk) > 1:
            table_data += _create_single_dish_table_data(dish_chunk[1])
            row_heights += [
                name_height,
                price_height,
                small_portion_height,
            ]
        content.append(
            Table(
                data=table_data,
                style=table_style,
                colWidths=["100%"],
                rowHeights=row_heights,
            )
        )
        content.append(PageBreak())

    doc = SimpleDocTemplate(
        get_postings_file_path(),
        pagesize=A4,
        leftMargin=10,
        topMargin=10,
        rightMargin=10,
        bottomMargin=10,
    )
    doc.build(content)
