from datetime import date
from typing import List, Dict

from core.enums import Weekday
from reportlab.lib.units import mm
from reportlab.platypus import SimpleDocTemplate, Paragraph, Table, Spacer

from menuplanner.models import Dish
from menuplanner.pdf.handout_styles import A4StyleSet, DoubleA5StyleSet


def _get_shops_text(shops, singular="das Geschäft", plural="die Geschäfte"):
    if len(shops) > 1:
        first = plural
        second = " und ".join([", ".join(shops[0:-1]), shops[-1]])
    else:
        first = singular
        second = shops[0]
    return f"{first} {second}"


def _generate_header(style, from_date, to_date, only_for_shops, except_for_shops):
    header_text = [
        Paragraph(
            f"Speiseplan für die Woche vom",
            style.header_text_style,
        ),
        Paragraph(
            f"<b>{from_date:%d.%m.} - {to_date:%d.%m.%Y}</b>",
            style.header_date_style,
        ),
    ]

    if only_for_shops:
        text = _get_shops_text([f"<b>{s}</b>" for s in only_for_shops])
        header_text.append(Paragraph(f"für {text}", style.header_text_style))

    if except_for_shops:
        text = _get_shops_text([f"<b>{s}</b>" for s in except_for_shops])
        header_text.append(Paragraph(f"<u>außer</u> für {text}", style.header_text_style))

    return Table(
        data=[[style.logo_image, header_text]],
        colWidths=style.header_table_col_widths,
        style=style.header_table_style,
    )


def _generate_content(style, dishes, weekdays_except_for_shops):
    table_data = []
    for day in Weekday.work_days():
        weekday_dishes = [d for d in dishes if d is not None and d.weekday == day]
        weekday_text = f"<b>{day.label}</b>"
        if shops := weekdays_except_for_shops.get(day):
            text = _get_shops_text(shops, singular="Geschäft", plural="Geschäfte")
            weekday_text += f" (außer {text})"
        table_data.append(
            [
                [
                    Spacer(width=10 * mm, height=style.pre_weekday_space),
                    Paragraph(weekday_text, style.weekday_text_style),
                ],
                None,
            ]
        )
        if weekday_dishes:
            for dish in weekday_dishes:
                table_data.append(
                    [
                        Paragraph(dish.name, style.normal_text_style),
                        Paragraph(dish.get_price_string(), style.price_text_style),
                    ]
                )
        else:
            table_data.append(
                [
                    Paragraph("-", style.normal_text_style),
                    None,
                ]
            )
    return Table(
        data=table_data,
        colWidths=style.content_table_col_widths,
        style=style.content_table_style,
        spaceBefore=style.content_table_space_before,
        spaceAfter=style.content_table_space_after,
    )


def _generate_page_data(style, dishes, from_date, to_date, only_for_shops, except_for_shops, weekdays_except_for_shops):
    header = _generate_header(style, from_date, to_date, only_for_shops, except_for_shops)
    content = _generate_content(style, dishes, weekdays_except_for_shops or {})
    footer = Paragraph("Änderungen vorbehalten", style.small_text_style)

    page_data = [header, content, footer]

    return page_data


def generate_handout_a5(
    dishes: List[Dish],
    from_date: date,
    to_date: date,
    only_for_shops: List[str] = None,
    except_for_shops: List[str] = None,
    weekdays_except_for_shops: Dict[Weekday, List[str]] = None,
):
    style = DoubleA5StyleSet()
    single_page_data = _generate_page_data(
        style,
        dishes,
        from_date,
        to_date,
        only_for_shops,
        except_for_shops,
        weekdays_except_for_shops,
    )
    table_row = [single_page_data, None, None, single_page_data]
    page_data = [
        Table(
            data=[table_row],
            colWidths=style.surrounding_table_col_widths,
            style=style.surrounding_table_style,
        )
    ]
    doc = SimpleDocTemplate(**style.document_properties)
    doc.build(page_data)


def generate_handout_a4(
    dishes: List[Dish],
    from_date: date,
    to_date: date,
    only_for_shops: List[str] = None,
    except_for_shops: List[str] = None,
    weekdays_except_for_shops: Dict[Weekday, List[str]] = None,
):
    style = A4StyleSet()
    page_data = _generate_page_data(
        style,
        dishes,
        from_date,
        to_date,
        only_for_shops,
        except_for_shops,
        weekdays_except_for_shops,
    )
    doc = SimpleDocTemplate(**style.document_properties)
    doc.build(page_data)
