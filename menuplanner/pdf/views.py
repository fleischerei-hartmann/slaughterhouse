from typing import Optional

from core.enums import Weekday
from django.http import HttpResponse
from django.views import View
from django.views.generic import TemplateView

from menuplanner.models import Dish, WeekMenu
from menuplanner.pdf.handout import generate_handout_a4, generate_handout_a5
from menuplanner.pdf.list import generate_list
from menuplanner.pdf.postings import generate_postings
from menuplanner.utils import (
    get_postings_file_path,
    get_list_file_path,
    build_error_html_response,
    get_objects_from_query_param,
    get_handout_a4_file_path,
    get_handout_a5_file_path,
    get_menu_date_range,
    get_object_from_query_param,
)


class PdfChoiceView(TemplateView):
    template_name = "menuplanner/pdf_choice.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        menu: Optional[WeekMenu] = None
        dishes: list[Dish] = []
        errors: list[str] = []
        if menu_id := self.request.GET.get("menu_id"):
            menu, query_error = get_object_from_query_param(menu_id, WeekMenu)
            if query_error:
                errors.append(query_error)
        elif dish_ids := self.request.GET.get("dish_ids"):
            dishes, query_errors = get_objects_from_query_param(dish_ids, Dish)
            errors.extend(query_errors)
        else:
            errors.append("no query param 'menu_id' nor 'dish_ids' was provided")

        dish_ids_query = ",".join(str(dish.pk) for dish in dishes)
        context.update(
            {
                "site_title": "Fleischerei Hartmann",
                "site_header": "Speiseplan-PDF",
                "title": "Speiseplan-PDF generieren",
                "menu": menu,
                "dishes": dishes,
                "errors": errors,
                "dish_ids_query": dish_ids_query,
            }
        )
        return context


class PdfListView(View):
    def get(self, request):
        dish_ids_query = self.request.GET.get("dish_ids")
        dishes, error = get_objects_from_query_param(dish_ids_query, Dish)
        if dishes:
            generate_list(dishes)
            with open(get_list_file_path(), "rb") as file:
                response = HttpResponse(file, content_type="application/pdf")
                response["Content-Disposition"] = "attachment; filename=speiseplan-liste.pdf"
                return response
        elif error:
            return build_error_html_response(f"Ein Fehler ist aufgetreten: {error}")
        return build_error_html_response("keine Gerichte ausgewählt")


class PdfPostingsView(View):
    def get(self, request):
        dish_ids_query = self.request.GET.get("dish_ids")
        dishes, error = get_objects_from_query_param(dish_ids_query, Dish)
        if dishes:
            generate_postings(dishes)
            with open(get_postings_file_path(), "rb") as file:
                response = HttpResponse(file, content_type="application/pdf")
                response["Content-Disposition"] = "attachment; filename=speiseplan-aushang.pdf"
                return response
        elif error:
            return build_error_html_response(f"Ein Fehler ist aufgetreten: {error}")
        return build_error_html_response("keine Gerichte ausgewählt")


class PdfHandoutA5View(View):
    def get(self, request):
        dish_ids_query = self.request.GET.get("dish_ids")
        dishes, error = get_objects_from_query_param(dish_ids_query, Dish)

        if dishes:
            # TODO make defaults configurable on the intermediate page
            monday, friday = get_menu_date_range()
            weekdays_except_for_shops = {Weekday.monday: ["Göhren"]}
            generate_handout_a5(
                dishes=dishes,
                from_date=monday,
                to_date=friday,
                except_for_shops=[],
                weekdays_except_for_shops=weekdays_except_for_shops,
            )
            with open(get_handout_a5_file_path(), "rb") as file:
                response = HttpResponse(file, content_type="application/pdf")
                response["Content-Disposition"] = "attachment; filename=speiseplan-a5.pdf"
                return response
        elif error:
            return build_error_html_response(f"Ein Fehler ist aufgetreten: {error}")
        return build_error_html_response("keine Gerichte ausgewählt")


class PdfHandoutA4View(View):
    def get(self, request):
        dish_ids_query = self.request.GET.get("dish_ids")
        dishes, error = get_objects_from_query_param(dish_ids_query, Dish)

        if dishes:
            # TODO make defaults configurable with another intermediate page
            monday, friday = get_menu_date_range()
            generate_handout_a4(
                dishes=dishes,
                from_date=monday,
                to_date=friday,
                only_for_shops=[],
            )
            with open(get_handout_a4_file_path(), "rb") as file:
                response = HttpResponse(file, content_type="application/pdf")
                response["Content-Disposition"] = "attachment; filename=speiseplan-a4.pdf"
                return response
        elif error:
            return build_error_html_response(f"Ein Fehler ist aufgetreten: {error}")
        return build_error_html_response("keine Gerichte ausgewählt")
