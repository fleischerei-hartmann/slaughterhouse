from datetime import date, timedelta
from typing import Type, List, Tuple

from django.db.models import Model
from django.template.loader import get_template
from django.template.response import SimpleTemplateResponse
from django.utils import timezone


def chunked(lst: List, size: int):
    for start in range(0, len(lst), size):
        yield lst[start : start + size]


def get_menu_date_range() -> Tuple[date, date]:
    now = timezone.now()
    today = now.date()
    if (day := today.weekday()) > 4:  # Saturday or Sunday
        past_work_week = True
    elif day == 4 and now.hour >= 14:  # Friday past 2pm
        past_work_week = True
    else:
        past_work_week = False
    if past_work_week:
        monday = today + timedelta(days=7 - today.weekday())
    else:
        monday = today - timedelta(days=today.weekday())
    friday = monday + timedelta(days=4)
    return monday, friday


def get_postings_file_path():
    return "/tmp/postings.pdf"


def get_list_file_path():
    return "/tmp/list.pdf"


def get_handout_a4_file_path():
    return "/tmp/handout-a4.pdf"


def get_handout_a5_file_path():
    return "/tmp/handout-a5.pdf"


def build_error_html_response(message: str, status=400):
    return SimpleTemplateResponse(
        template=get_template("menuplanner/general_error.html"),
        context={
            "site_title": "Fleischerei Hartmann",
            "site_header": "Speiseplanverwaltung",
            "title": "Fehler",
            "message": message,
        },
        status=status,
    )


def get_object_from_query_param(query_param: str, model: Type[Model]) -> (Model, str):
    obj, error = None, None
    if query_param:
        try:
            obj = model.objects.get(pk=int(query_param.strip()))
        except ValueError:
            error = f"'{query_param}' is not a valid number"
        except model.DoesNotExist:
            error = f"{model.__name__} with ID {query_param} does not exist"
    else:
        error = f"no {model.__name__} ID was provided"
    return obj, error


def get_objects_from_query_param(query_param: str, model: Type[Model]) -> (list[Model], list[str]):
    objects, errors = [], []
    if query_param:
        for part in query_param.split(","):
            obj, err = get_object_from_query_param(part, model)
            if obj:
                objects.append(obj)
            if err:
                errors.append(err)
    return objects, errors
