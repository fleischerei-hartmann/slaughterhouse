from datetime import timedelta
from html import escape
from typing import List, Optional

from core.enums import Weekday
from django.template.loader import get_template
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.views.generic import TemplateView

from menuplanner.models import Dish
from menuplanner.utils import get_objects_from_query_param


def _get_dish_for_weekday(dishes: List[Dish], day: Weekday) -> Optional[Dish]:
    relevant_dishes = [d for d in dishes if d.weekday == day]
    if not relevant_dishes:
        print(f"no dish found for weekday {day.name}")
        return None
    if (amount := len(relevant_dishes)) > 1:
        print(f"multiple dishes ({amount}) found for weekday {day.name}")
    return relevant_dishes[0]


def generate_wordpress_html(dishes: List[Dish]):
    today = timezone.now().date()
    next_monday = today + timedelta(days=7 - today.weekday())
    next_friday = next_monday + timedelta(days=4)
    context = {
        "date_range_str": f"{next_monday:%d.%m.} - {next_friday:%d.%m.%Y}",
        "dish_mo": _get_dish_for_weekday(dishes, Weekday.monday),
        "dish_tu": _get_dish_for_weekday(dishes, Weekday.tuesday),
        "dish_we": _get_dish_for_weekday(dishes, Weekday.wednesday),
        "dish_th": _get_dish_for_weekday(dishes, Weekday.thursday),
        "dish_fr": _get_dish_for_weekday(dishes, Weekday.friday),
    }
    template = get_template("menuplanner/wordpress.html")
    return template.render(context)


class WordpressHtmlView(TemplateView):
    template_name = "menuplanner/wordpress-meta.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        dish_id_query = self.request.GET.get("dish_ids")
        dishes, _, _ = get_objects_from_query_param(dish_id_query, Dish)
        content = generate_wordpress_html(dishes)
        context.update(
            {
                "site_title": "Fleischerei Hartmann",
                "site_header": "Speiseplanverwaltung",
                "title": "Wordpress-HTML",
                "content": mark_safe(escape(content)),
            }
        )
        return context
