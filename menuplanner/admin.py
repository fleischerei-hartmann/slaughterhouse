from core.enums import Weekday
from django.contrib.admin import ModelAdmin, register
from django.http import HttpResponseRedirect
from django.urls import reverse, path

from menuplanner.models import Dish, WeekMenu
from menuplanner.pdf.views import PdfChoiceView, PdfListView, PdfPostingsView, PdfHandoutA4View, PdfHandoutA5View
from menuplanner.wordpress.views import WordpressHtmlView
from usermgmt.admin import admin_site
from warehousing.utils import format_price


def get_week_number(dish):
    return dish.week_number


get_week_number.short_description = "Woche"


def get_weekday_string(dish):
    return Weekday(dish.weekday).label[:2]


get_weekday_string.short_description = "Tag"


def get_price(dish):
    return format_price(dish.price)


get_price.short_description = "Preis"


def _generate_pdf(modeladmin, request, queryset):
    query = ",".join(str(dish.pk) for dish in queryset)
    url = reverse("admin:dish_pdf_choice") + "?dish_ids=" + query
    return HttpResponseRedirect(url)


_generate_pdf.short_description = "PDF generieren ..."


def _generate_wordpress_html(modeladmin, request, queryset):
    query = ",".join(str(dish.pk) for dish in queryset)
    url = reverse("admin:dish_wordpress") + "?dish_ids=" + query
    return HttpResponseRedirect(url)


_generate_wordpress_html.short_description = "WordPress-HTML generieren"


@register(Dish, site=admin_site)
class DishAdmin(ModelAdmin):
    list_display = (
        get_week_number,
        get_weekday_string,
        "name",
        get_price,
    )
    list_display_links = ("name",)
    search_fields = ("name",)
    list_filter = ("week_number",)
    actions = (_generate_pdf, _generate_wordpress_html)

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            path(
                "pdf-choice/",
                self.admin_site.admin_view(PdfChoiceView.as_view()),
                name="dish_pdf_choice",
            ),
            path(
                "pdf-list/",
                self.admin_site.admin_view(PdfListView.as_view()),
                name="dish_pdf_list",
            ),
            path(
                "pdf-postings/",
                self.admin_site.admin_view(PdfPostingsView.as_view()),
                name="dish_pdf_postings",
            ),
            path(
                "pdf-handout-a5/",
                self.admin_site.admin_view(PdfHandoutA5View.as_view()),
                name="dish_pdf_handout_a5",
            ),
            path(
                "pdf-handout-a4/",
                self.admin_site.admin_view(PdfHandoutA4View.as_view()),
                name="dish_pdf_handout_a4",
            ),
            path(
                "wordpress/",
                self.admin_site.admin_view(WordpressHtmlView.as_view()),
                name="dish_wordpress",
            ),
        ]
        return custom_urls + urls


def _generate_pdf_from_menu(modeladmin, request, queryset):
    url = f"{reverse('admin:dish_pdf_choice')}?menu_id={queryset.get().pk}"
    return HttpResponseRedirect(url)


_generate_pdf_from_menu.short_description = "PDF generieren ..."


@register(WeekMenu, site=admin_site)
class WeekMenuAdmin(ModelAdmin):
    autocomplete_fields = ("dish_mon", "dish_tue", "dish_wed", "dish_thu", "dish_fri")
    actions = (_generate_pdf_from_menu,)
