from datetime import timedelta
from decimal import Decimal

from core.enums import Weekday
from django.core.exceptions import ValidationError
from django.db import models


class Dish(models.Model):
    class Meta:
        verbose_name = "Gericht"
        verbose_name_plural = "Gerichte"
        unique_together = ("week_number", "weekday")
        ordering = ("week_number", "weekday")

    name = models.CharField("Name", max_length=100)
    price = models.DecimalField("Preis", max_digits=4, decimal_places=2)
    week_number = models.SmallIntegerField("Wochennummer")
    weekday = models.PositiveSmallIntegerField(choices=Weekday.choices, verbose_name="Wochentag")

    @property
    def small_price(self):
        price = self.price * Decimal("0.7")
        price = price.quantize(Decimal("0.0"))
        return price.quantize(Decimal("0.00"))

    def get_price_string(self):
        return f"{self.price} €".replace(".", ",")

    def get_small_price_string(self):
        return f"{self.small_price} €".replace(".", ",")

    def __str__(self):
        return self.name


def validate_monday(value):
    if not hasattr(value, "weekday"):
        raise ValidationError(f"value {value} of type {type(value)} has no weekday attribute")
    if value.weekday() != 0:
        raise ValidationError(f"date {value} is not a Monday")


class WeekMenu(models.Model):
    class Meta:
        verbose_name = "Wochenplan"
        verbose_name_plural = "Wochenpläne"

    start_date = models.DateField(
        unique=True,
        validators=[validate_monday],
        verbose_name="Anfangsdatum",
        help_text="Datum von Montag, auch wenn es ein Feiertag ist",
    )
    dish_mon = models.ForeignKey(
        to=Dish,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name="Montagsgericht",
        related_name="+",
    )
    dish_tue = models.ForeignKey(
        to=Dish,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name="Dienstagsgericht",
        related_name="+",
    )
    dish_wed = models.ForeignKey(
        to=Dish,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name="Mittwochsgericht",
        related_name="+",
    )
    dish_thu = models.ForeignKey(
        to=Dish,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name="Donnerstagsgericht",
        related_name="+",
    )
    dish_fri = models.ForeignKey(
        to=Dish,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name="Freitagsgericht",
        related_name="+",
    )

    @property
    def end_date(self):
        return self.start_date + timedelta(days=4)

    @property
    def dishes(self):
        for day in ("mon", "tue", "wed", "thu", "fri"):
            yield getattr(self, f"dish_{day}")

    def __str__(self):
        return f"{self.start_date:%d.%m.} - {self.end_date:%d.%m.%Y}"
