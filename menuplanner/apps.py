from django.apps import AppConfig


class MenuplannerConfig(AppConfig):
    name = "menuplanner"
    verbose_name = "Speiseplanverwaltung"
