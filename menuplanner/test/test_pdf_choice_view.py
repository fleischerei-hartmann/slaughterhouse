from decimal import Decimal

from core.enums import Weekday
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from menuplanner.models import Dish

User = get_user_model()


class TestPdfChoiceView(TestCase):
    url = reverse("admin:dish_pdf_choice")

    def test_not_authenticated(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_no_staff_user(self):
        user = User.objects.create(username="foobar", password="supersafe")
        self.client.force_login(user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_successful(self):
        user = User.objects.create(username="foobar", password="supersafe", is_staff=True)
        self.client.force_login(user)
        dish1 = Dish.objects.create(
            name="Käse-Lauch-Suppe",
            price=Decimal("5.50"),
            week_number=1,
            weekday=Weekday.monday,
        )
        dish2 = Dish.objects.create(
            name="vegane Käse-Lauch-Suppe",
            price=Decimal("4.80"),
            week_number=1,
            weekday=Weekday.tuesday,
        )
        query = f"dish_ids={dish1.pk},{dish2.pk}"
        response = self.client.get(self.url + "?" + query)
        self.assertEqual(response.status_code, 200)

    def test_bad_query_param(self):
        user = User.objects.create(username="foobar", password="supersafe", is_staff=True)
        self.client.force_login(user)
        response = self.client.get(self.url + "?dish_ids=foo")
        self.assertEqual(response.status_code, 200)
