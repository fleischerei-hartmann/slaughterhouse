from django.contrib.auth.models import AbstractUser

from usermgmt.modelutils import AutoCleanModel


class User(AbstractUser, AutoCleanModel):
    class Meta(AbstractUser.Meta):
        verbose_name = "Nutzer"
        verbose_name_plural = "Nutzer"

    def __str__(self):
        return self.username
