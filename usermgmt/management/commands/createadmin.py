from django.contrib.auth.management.commands import createsuperuser
from django.db import transaction


class Command(createsuperuser.Command):
    help = "Used to create the superuser 'flo' with password '123'"

    @transaction.atomic()
    def handle(self, *args, **options):
        options.update(
            {
                self.UserModel.USERNAME_FIELD: "flo",
                "email": "flo@test.com",
                "interactive": False,
            }
        )
        super().handle(*args, **options)
        user = self.UserModel.objects.latest("created_at")
        user.set_password("123")
        user.save()
