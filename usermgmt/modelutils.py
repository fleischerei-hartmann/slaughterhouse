import logging

from django.core.exceptions import ValidationError
from django.db import IntegrityError, models

logger = logging.getLogger(__name__)

unique_violation_msg = "duplicate key value violates unique constraint"


class AutoCleanModel(models.Model):
    class Meta:
        abstract = True

    created_at = models.DateTimeField("erstellt am", auto_now_add=True)
    modified_at = models.DateTimeField("bearbeitet am", auto_now=True)

    def save(self, *args, **kwargs):
        self.full_clean()
        try:
            super().save(*args, **kwargs)
        except IntegrityError as err:
            if unique_violation_msg in str(err):
                logger.warning("converting an unique violation IntegrityError to ValidationError")
                raise ValidationError("object cannot be stored due to unique field violation")
            else:
                raise

    def __str__(self):
        return str(self.pk)
