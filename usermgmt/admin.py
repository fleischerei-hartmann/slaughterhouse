from django.contrib.admin import AdminSite as DjangoAdminSite, register
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin

User = get_user_model()


class AdminSite(DjangoAdminSite):
    index_title = "Fleischerei Hartmann"
    site_title = "Admin Panel"
    site_header = "Admin Panel"


admin_site = AdminSite()


@register(User, site=admin_site)
class UserAdmin(DjangoUserAdmin):
    list_display = (
        "username",
        "is_superuser",
        "last_login",
    )
    list_filter = tuple()
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "username",
                    "password",
                    "is_staff",
                    "is_superuser",
                    "user_permissions",
                    "last_login",
                )
            },
        ),
    )
    add_fieldsets = (
        (
            None,
            {
                "fields": (
                    "username",
                    "password1",
                    "password2",
                    "is_staff",
                    "is_superuser",
                    "user_permissions",
                )
            },
        ),
    )
    readonly_fields = ("last_login",)
