FROM python:3.13
ENV PYTHONUNBUFFERED=1
WORKDIR /code/
COPY requirements.txt .
RUN apt-get update && \
    apt-get upgrade --assume-yes && \
    pip install --upgrade pip && \
    pip install --no-cache-dir --requirement requirements.txt
COPY . .
ENTRYPOINT ["./entrypoint.sh"]
