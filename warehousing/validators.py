from django.core.exceptions import ValidationError


def validate_numeric(value):
    if type(value) is not str or not value.isnumeric():
        raise ValidationError(f"{value} is not a numeric string")


def validate_positive(value):
    if value < 0:
        raise ValidationError(f"{value} is not positive")
