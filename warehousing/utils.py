from decimal import Decimal

from django.utils.formats import localize

from warehousing.models import Booking, Warehouse, Article


def round_price(dec_num):
    if dec_num is None:
        return None
    return dec_num.quantize(Decimal(10) ** -2)


def format_price(dec_num):
    if dec_num is None:
        return "-"
    return f"{localize(round_price(dec_num))} €"


def format_amount(dec_num):
    result = localize(dec_num)
    while "," in result and result.endswith("0"):
        result = result[: len(result) - 1]
    if result.endswith(","):
        result = result[: len(result) - 1]
    return result


def _get_objects_for_pks(warehouse_pk, article_pk):
    return Warehouse.objects.get(pk=warehouse_pk), Article.objects.get(pk=article_pk)


def log_add_booking(user, item_instance):
    Booking.objects.create(
        warehouse=item_instance.warehouse,
        article=item_instance.article,
        inbound=True,
        amount=item_instance.amount,
        user=user,
    )


def log_delete_booking(user, item_instance=None, item_dict=None):
    if (item_instance and item_dict) or (not item_instance and not item_dict):
        raise AssertionError("instance or dict should be provided, not both or none of them")
    if item_instance:
        warehouse, article = item_instance.warehouse, item_instance.article
        amount = item_instance.amount
    else:
        warehouse, article = _get_objects_for_pks(item_dict["warehouse"], item_dict["article"])
        amount = item_dict["amount"]
    Booking.objects.create(
        warehouse=warehouse,
        article=article,
        inbound=False,
        amount=amount,
        user=user,
    )


def log_change_booking(user, old_item_dict, new_item_instance):
    old_warehouse, old_article = _get_objects_for_pks(old_item_dict["warehouse"], old_item_dict["article"])
    old_amount = old_item_dict["amount"]
    new_warehouse = new_item_instance.warehouse
    new_article = new_item_instance.article
    new_amount = new_item_instance.amount
    if old_warehouse != new_warehouse or old_article != new_article:
        log_delete_booking(user, item_dict=old_item_dict)
        log_add_booking(user, item_instance=new_item_instance)
    else:
        inbound = new_amount > old_amount
        amount_diff = abs(new_amount - old_amount)
        Booking.objects.create(
            warehouse=new_warehouse,
            article=new_article,
            inbound=inbound,
            amount=amount_diff,
            user=user,
        )
