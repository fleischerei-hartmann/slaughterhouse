from django.apps import AppConfig


class WarehousingConfig(AppConfig):
    name = "warehousing"
    verbose_name = "Lagerverwaltung"
