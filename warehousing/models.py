from decimal import Decimal

from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.utils.formats import localize

from usermgmt.modelutils import AutoCleanModel
from warehousing.validators import validate_numeric, validate_positive

User = get_user_model()


class Warehouse(AutoCleanModel):
    class Meta:
        verbose_name = "Lager"
        verbose_name_plural = "Lager"
        ordering = ("name",)

    name = models.CharField("Name", unique=True, max_length=50)

    def __str__(self):
        return self.name

    @property
    def item_count(self):
        return self.items.all().count()

    @property
    def total_item_price(self):
        if self.items.exists():
            return sum(item.price for item in self.items.all())
        else:
            return Decimal("0")

    @property
    def last_action(self):
        try:
            last_booking = self.bookings.latest("created_at").created_at
        except ObjectDoesNotExist:
            last_booking = None
        if last_booking:
            return max(self.modified_at, last_booking)
        return self.modified_at


class Unit(AutoCleanModel):
    class Meta:
        verbose_name = "Einheit"
        verbose_name_plural = "Einheiten"
        ordering = ("name",)

    name = models.CharField("Name", unique=True, max_length=30)

    def __str__(self):
        return self.name


class Article(AutoCleanModel):
    class Meta:
        verbose_name = "Artikel"
        verbose_name_plural = "Artikel"
        ordering = (
            "plu",
            "name",
        )

    name = models.CharField("Name", unique=True, max_length=70)
    plu = models.CharField("PLU", blank=True, default="", max_length=4, validators=(validate_numeric,))
    unit = models.ForeignKey(verbose_name="Einheit", to=Unit, on_delete=models.PROTECT, related_name="+")
    price_per_unit = models.DecimalField(
        "Preis pro Einheit",
        max_digits=10,
        decimal_places=4,
        default=Decimal("0"),
    )

    def __str__(self):
        return self.name


class Item(AutoCleanModel):
    class Meta:
        verbose_name = "Ware"
        verbose_name_plural = "Waren"
        unique_together = ("warehouse", "article")

    warehouse = models.ForeignKey(verbose_name="Lager", to=Warehouse, on_delete=models.PROTECT, related_name="items")
    article = models.ForeignKey(verbose_name="Artikel", to=Article, on_delete=models.PROTECT, related_name="items")
    amount = models.DecimalField("Anzahl", max_digits=9, decimal_places=3, validators=(validate_positive,))

    def __str__(self):
        return f"{localize(self.amount)} {self.article.unit} {self.article}"

    @property
    def price(self):
        return self.amount * self.article.price_per_unit


class Booking(AutoCleanModel):
    class Meta:
        verbose_name = "Buchung"
        verbose_name_plural = "Buchungen"
        ordering = ("-created_at",)

    warehouse = models.ForeignKey(verbose_name="Lager", to=Warehouse, on_delete=models.CASCADE, related_name="bookings")
    article = models.ForeignKey(verbose_name="Artikel", to=Article, on_delete=models.CASCADE, related_name="bookings")
    inbound = models.BooleanField("eingehend")
    amount = models.DecimalField("Menge", max_digits=9, decimal_places=3, validators=(validate_positive,))
    user = models.ForeignKey(
        verbose_name="Nutzer",
        to=User,
        null=True,
        on_delete=models.SET_NULL,
        related_name="bookings",
    )

    def __str__(self):
        return f"{'IN' if self.inbound else 'OUT'} {localize(self.amount)} {self.article.unit} {self.article}"
