from django.contrib.admin import register, ModelAdmin

from usermgmt.admin import admin_site
from warehousing.models import Booking
from warehousing.utils import format_amount


def get_amount(booking):
    return f"{format_amount(booking.amount)} {booking.article.unit}"


get_amount.short_description = "Menge"


@register(Booking, site=admin_site)
class BookingAdmin(ModelAdmin):
    list_display = ("warehouse", "article", "inbound", get_amount, "user", "created_at")
    list_display_links = None
    search_fields = ("article__plu", "article__name")
    ordering = ("-created_at",)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False
