from django.contrib.admin import register, ModelAdmin

from usermgmt.admin import admin_site
from warehousing.models import Warehouse
from warehousing.utils import format_price


def get_item_count(warehouse):
    return warehouse.item_count


get_item_count.short_description = "Warenanzahl"


def get_total_item_price(warehouse):
    return format_price(warehouse.total_item_price)


get_total_item_price.short_description = "Warenwert"


@register(Warehouse, site=admin_site)
class WarehouseAdmin(ModelAdmin):
    list_display = ("name", get_item_count, get_total_item_price)
    list_display_links = ("name",)
    ordering = ("name",)
    fields = ("name", get_item_count, get_total_item_price)
    readonly_fields = (get_item_count, get_total_item_price)
