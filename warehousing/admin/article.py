from django.contrib.admin import register, ModelAdmin

from usermgmt.admin import admin_site
from warehousing.models import Article
from warehousing.utils import format_price


def get_price_per_unit(article):
    return format_price(article.price_per_unit)


get_price_per_unit.short_description = "Preis / Einheit"
get_price_per_unit.admin_order_field = "price_per_unit"


@register(Article, site=admin_site)
class ArticleAdmin(ModelAdmin):
    list_display = ("plu", "name", "unit", get_price_per_unit)
    list_display_links = ("plu", "name")
    search_fields = ("plu", "name")
    ordering = ("plu", "name")
