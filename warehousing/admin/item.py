from django.contrib.admin import register, ModelAdmin
from django.db import transaction

from usermgmt.admin import admin_site
from warehousing.models import Item
from warehousing.utils import format_price, log_change_booking, log_add_booking, log_delete_booking, format_amount


def get_article_plu(item):
    return item.article.plu


get_article_plu.short_description = "PLU"
get_article_plu.admin_order_field = "article__plu"


def get_article_name(item):
    return item.article.name


get_article_name.short_description = "Artikel"
get_article_name.admin_order_field = "article__name"


def get_amount(item):
    return f"{format_amount(item.amount)} {item.article.unit}"


get_amount.short_description = "Menge"


def get_price(item):
    return format_price(item.price)


get_price.short_description = "Preis"


@register(Item, site=admin_site)
class ItemAdmin(ModelAdmin):
    list_display = (get_article_plu, get_article_name, "warehouse", get_amount, get_price)
    list_display_links = (get_article_plu, get_article_name)
    search_fields = ("article__plu", "article__name")
    ordering = ("article__plu", "article__name")
    autocomplete_fields = ("article",)

    @transaction.atomic()
    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        if change:
            log_change_booking(request.user, old_item_dict=form.initial, new_item_instance=obj)
        else:
            log_add_booking(request.user, item_instance=obj)

    @transaction.atomic()
    def delete_model(self, request, obj):
        log_delete_booking(request.user, item_instance=obj)
        super().delete_model(request, obj)

    @transaction.atomic()
    def delete_queryset(self, request, queryset):
        for obj in queryset:
            self.delete_model(request, obj)
