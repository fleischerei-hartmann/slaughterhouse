from django.contrib.admin import register, ModelAdmin

from usermgmt.admin import admin_site
from warehousing.models import Unit


@register(Unit, site=admin_site)
class UnitAdmin(ModelAdmin):
    list_display = ("name",)
    list_display_links = ("name",)
    ordering = ("name",)
