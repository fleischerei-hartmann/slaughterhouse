import os

from django.urls import reverse_lazy


def _get_env_production_mode():
    env_var = os.environ.get("DJANGO_PRODUCTION_MODE", "false")
    return env_var.lower() == "true"


def _get_env_secret_key():
    env_var = os.environ.get("DJANGO_SECRET_KEY")
    assert env_var is not None, "DJANGO_SECRET_KEY environment variable must be set when using production mode"
    return env_var


def _get_env_allowed_hosts():
    env_var = os.environ.get("DJANGO_ALLOWED_HOSTS")
    assert env_var is not None, "DJANGO_ALLOWED_HOSTS environment variable must be set when using production mode"
    return [host.strip() for host in env_var.split(",")]


def _get_env_static_root():
    env_var = os.environ.get("DJANGO_STATIC_ROOT")
    assert env_var is not None, "DJANGO_STATIC_ROOT environment variable must be set when using production mode"
    return env_var


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

PRODUCTION = _get_env_production_mode()

if PRODUCTION:
    SECRET_KEY = _get_env_secret_key()
    DEBUG = False
    ALLOWED_HOSTS = _get_env_allowed_hosts()
    CSRF_TRUSTED_ORIGINS = [f"https://{host}" for host in ALLOWED_HOSTS]
    STATIC_ROOT = _get_env_static_root()
else:
    SECRET_KEY = "BNylaNqiKeIIl7glKH07c1ykdKQmYbXubJ6C6WmN7jHB9927lR"
    DEBUG = True
    ALLOWED_HOSTS = ["*"]

ENABLE_SHOP_BETA = os.environ.get("ENABLE_SHOP_BETA") == "true"

INSTALLED_APPS = [
    "core.apps.CoreConfig",
    "warehousing.apps.WarehousingConfig",
    "menuplanner.apps.MenuplannerConfig",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "usermgmt.apps.UsermgmtConfig",
]

if ENABLE_SHOP_BETA:
    INSTALLED_APPS.extend(
        [
            "shop.apps.ShopConfig",
            "legacy.apps.LegacyConfig",
        ]
    )

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "slaughterhouse.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "slaughterhouse.wsgi.application"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "HOST": os.environ["DB_HOST"],
        "PORT": os.environ["DB_PORT"],
        "NAME": os.environ["DB_NAME"],
        "USER": os.environ["DB_USER"],
        "PASSWORD": os.environ["DB_PASSWORD"],
    }
}

if ENABLE_SHOP_BETA:
    DATABASES.update(
        {
            "legacy": {
                "ENGINE": "django.db.backends.mysql",
                "HOST": os.environ["LEGACY_DB_HOST"],
                "PORT": os.environ["LEGACY_DB_PORT"],
                "NAME": os.environ["LEGACY_DB_NAME"],
                "USER": os.environ["LEGACY_DB_USER"],
                "PASSWORD": os.environ["LEGACY_DB_PASSWORD"],
            }
        }
    )
    DATABASE_ROUTERS = ["legacy.db_router.LegacyDBRouter"]

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

AUTH_USER_MODEL = "usermgmt.User"
LOGIN_URL = reverse_lazy("admin:login")

LANGUAGE_CODE = "de"
TIME_ZONE = "Europe/Berlin"
USE_I18N = True
USE_L10N = True
USE_TZ = True
USE_THOUSAND_SEPARATOR = True

STATIC_URL = "/static/"
