from django.conf import settings
from django.urls import path, include

from usermgmt.admin import admin_site

urlpatterns = [
    path("admin/", admin_site.urls),
    path("", include("core.urls")),
]

if settings.ENABLE_SHOP_BETA:
    urlpatterns.append(path("shop-beta/", include("shop.urls")))
