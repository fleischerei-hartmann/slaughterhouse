from django.db import models


class Weekday(models.IntegerChoices):
    monday = 0, "Montag"
    tuesday = 1, "Dienstag"
    wednesday = 2, "Mittwoch"
    thursday = 3, "Donnerstag"
    friday = 4, "Freitag"
    saturday = 5, "Samstag"
    sunday = 6, "Sonntag"

    @classmethod
    def work_days(cls):
        for d in (
            cls.monday,
            cls.tuesday,
            cls.wednesday,
            cls.thursday,
            cls.friday,
        ):
            yield d
