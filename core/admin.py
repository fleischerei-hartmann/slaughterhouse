from django.contrib.admin import ModelAdmin, register

from core.models import Store, NewsPost
from usermgmt.admin import admin_site


@register(Store, site=admin_site)
class StoreAdmin(ModelAdmin):
    list_display = ("name",)
    list_display_links = ("name",)


@register(NewsPost, site=admin_site)
class NewsPostAdmin(ModelAdmin):
    list_display = ("heading", "published_date")
    ordering = ("-published_date",)
