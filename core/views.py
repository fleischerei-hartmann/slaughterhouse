from datetime import timedelta

from django.utils import timezone
from django.views.generic import TemplateView

from core.models import NewsPost
from menuplanner.models import WeekMenu


class HomeView(TemplateView):
    template_name = "core/home.html"


class HistoryView(TemplateView):
    template_name = "core/history.html"


class QualityView(TemplateView):
    template_name = "core/quality.html"


class NewsView(TemplateView):
    template_name = "core/news.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        threshold = timezone.now() - timedelta(days=180)
        recent_posts = NewsPost.objects.filter(published_date__gt=threshold).order_by("-published_date")[:5]
        context.update({"posts": recent_posts})
        return context


class MenuView(TemplateView):
    template_name = "core/menu.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # change to the new menu on Saturday, therefore look for weeks that start in 2 days or earlier
        two_days_ahead = timezone.now() + timedelta(days=2)
        try:
            menu = WeekMenu.objects.filter(start_date__lte=two_days_ahead.date()).latest("start_date")
        except WeekMenu.DoesNotExist:
            menu = None
        context.update({"menu": menu})
        return context


class PartyView(TemplateView):
    template_name = "core/party.html"


class ContactView(TemplateView):
    template_name = "core/contact.html"


class ImprintView(TemplateView):
    template_name = "core/imprint.html"
