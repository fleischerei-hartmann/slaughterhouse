from core.utils import today
from django.db import models


class Store(models.Model):
    class Meta:
        verbose_name = "Filiale"
        verbose_name_plural = "Filialen"

    name = models.CharField(max_length=64, unique=True, verbose_name="Name")
    street = models.CharField(max_length=64, verbose_name="Straße")
    number = models.CharField(max_length=8, verbose_name="Hausnummer")
    postal_code = models.CharField(max_length=8, verbose_name="Postleitzahl")
    place = models.CharField(max_length=64, verbose_name="Ort")

    def __str__(self):
        return self.name


class NewsPost(models.Model):
    heading = models.TextField()
    text = models.TextField()
    published_date = models.DateField(default=today)
