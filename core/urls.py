from django.urls import path

from . import views

urlpatterns = [
    path("", views.HomeView.as_view(), name="home"),
    path("geschichte/", views.HistoryView.as_view(), name="history"),
    path("qualitaet/", views.QualityView.as_view(), name="quality"),
    path("aktuelles/", views.NewsView.as_view(), name="news"),
    path("speiseplan/", views.MenuView.as_view(), name="menu"),
    path("partyservice/", views.PartyView.as_view(), name="party"),
    path("kontakt/", views.ContactView.as_view(), name="contact"),
    path("impressum/", views.ImprintView.as_view(), name="imprint"),
]
