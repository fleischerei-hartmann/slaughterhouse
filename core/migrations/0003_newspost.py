from django.db import migrations, models

import core.utils


class Migration(migrations.Migration):
    dependencies = [
        ('core', '0002_delete_openhours'),
    ]

    operations = [
        migrations.CreateModel(
            name='NewsPost',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('heading', models.TextField()),
                ('text', models.TextField()),
                ('published_date', models.DateField(default=core.utils.today)),
            ],
        ),
    ]
