# Generated by Django 3.1.3 on 2020-11-29 18:04

from django.db import migrations, models
import django.db.models.deletion
import django.db.models.expressions


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Store',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, unique=True, verbose_name='Name')),
                ('street', models.CharField(max_length=64, verbose_name='Straße')),
                ('number', models.CharField(max_length=8, verbose_name='Hausnummer')),
                ('postal_code', models.CharField(max_length=8, verbose_name='Postleitzahl')),
                ('place', models.CharField(max_length=64, verbose_name='Ort')),
            ],
            options={
                'verbose_name': 'Filiale',
                'verbose_name_plural': 'Filialen',
            },
        ),
        migrations.CreateModel(
            name='OpenHours',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('weekday', models.PositiveSmallIntegerField(choices=[(0, 'Montag'), (1, 'Dienstag'), (2, 'Mittwoch'), (3, 'Donnerstag'), (4, 'Freitag'), (5, 'Samstag'), (6, 'Sonntag')], verbose_name='Wochentag')),
                ('opens_at', models.TimeField(verbose_name='öffnet um')),
                ('closes_at', models.TimeField(verbose_name='schließt um')),
                ('store', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='open_hours', related_query_name='open_hour', to='core.store', verbose_name='Filiale')),
            ],
            options={
                'verbose_name': 'Öffnungszeit',
                'verbose_name_plural': 'Öffnungszeiten',
                'ordering': ('weekday',),
            },
        ),
        migrations.AddConstraint(
            model_name='openhours',
            constraint=models.CheckConstraint(check=models.Q(closes_at__gt=django.db.models.expressions.F('opens_at')), name='close_after_open'),
        ),
        migrations.AlterUniqueTogether(
            name='openhours',
            unique_together={('store', 'weekday')},
        ),
    ]
