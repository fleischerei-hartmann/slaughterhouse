#!/usr/bin/env bash
set -e

if [ "$DJANGO_PRODUCTION_MODE" == "true" ]; then
  echo "starting production server ..."
  gunicorn --bind=0.0.0.0:8000 slaughterhouse.wsgi

else
  echo "starting development server ..."
  python manage.py runserver 0.0.0.0:8000

fi
