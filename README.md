## local development

Just use the provided `docker-compose.yml`, start the database and application containers
with `docker compose up --build -d`, prepare the database with `docker compose exec web ./manage.py migrate` and
visit http://localhost in a browser. Create an admin with `docker compose exec web ./manage.py createsuperuser` and
visit the admin panel under http://localhost/admin to play around with some data (which is partly shown on the website).

## productive hosting

We recommend using your own `docker-compose.yml`, in which you should generate a nice password for the database and
change/add the following environment variables for the web container:

```yaml
services:

  db:
    image: postgres:16-alpine
    environment:
      POSTGRES_DB: "your-db-name"
      POSTGRES_USER: "your-db-user"
      POSTGRES_PASSWORD: "your-db-password"
    expose:
      - "5432"
    # ... configure volumes, restart etc.

  slaughterhouse:
    build:
      context: path-to-this-repo
    environment:
      DB_HOST: "db"
      DB_PORT: "5432"
      DB_NAME: "your-db-name"
      DB_USER: "your-db-user"
      DB_PASSWORD: "your-db-password"
      DJANGO_PRODUCTION_MODE: "true"
      DJANGO_SECRET_KEY: "your-secret-key"
      DJANGO_ALLOWED_HOSTS: "your-site.com,www.your-site.com"
      DJANGO_STATIC_ROOT: "/var/django-static/"
    depends_on:
      - db
    expose:
      - "8000"
    volumes:
      - ./volumes/slaughterhouse/static:/var/django-static:rw
    # ... configure restart etc.
    # and use a reverse proxy for ssl and to host static files
```
